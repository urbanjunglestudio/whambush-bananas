<?php

$api = 'https://api.whambush.com/v2.0/';

function http_post($endpoint,$data,$auth="0")
{
  global $api;
  $url = $api.$endpoint;
  $json = json_encode($data);

  if  ($auth != "0") {
    $header = "Content-type: application/x-www-form-urlencoded\r\nAuthorization: Token ".$auth."\r\n";
  } else {
    $header = "Content-type: application/x-www-form-urlencoded\r\n";
  }

  $options = array(
      'http' => array(
          'header'  => $header,
          'method'  => 'POST',
          'content' => http_build_query($data),
      ),
  );
  $context  = stream_context_create($options);
  $result = file_get_contents($url, false, $context);

  return json_decode($result,true);
}


function http_get($endpoint,$auth="0",$fullurl=FALSE)
{
  global $api;

  if ($fullurl) {
  	$url = $endpoint;
  } else {
  	$url = $api.$endpoint;
  }
  //$json = json_encode($data);

  if  ($auth != "0") {
    $header = "Content-type: application/x-www-form-urlencoded\r\nAuthorization: Token ".$auth."\r\n";
  } else {
    $header = "Content-type: application/x-www-form-urlencoded\r\n";
  }

  $options = array(
      'http' => array(
          'header'  => $header,
          'method'  => 'GET',
      ),
  );

  $context  = stream_context_create($options);
  $result = file_get_contents($url, false, $context);
  
  $finfo = new finfo(FILEINFO_MIME_TYPE);
  //print_r($finfo->buffer($result));
 
  return json_decode($result,true);
}

function login()
{

	echo "This script requires authentication".PHP_EOL;
	echo "Username: ";
	$username = trim(fgets(STDIN));

	echo "Password: ";
	system('stty -echo');
	$password = trim(fgets(STDIN));
	system('stty echo');
	// add a new line since the users CR didn't echo
	echo PHP_EOL;

	$data = array('username' => $username,
              	'password' => $password,
                'guest_id' => '1',
             	  'manufacturer'=>'jk',
             	  'os'=>'php',
            	  'os_version'=>'1',
            	  'device'=>'admin',
            	  'whambush_version'=>'1.3');

	$loginarray = http_post("login/",$data,0);

	if	(strlen($loginarray['token']) > 1) {
		echo "////////////////".PHP_EOL;
		return $loginarray;
	} else {
		echo "Login failed!".PHP_EOL;
		exit;
	}

}


/**
 * creating between two date
 * @param string since
 * @param string until
 * @param string step
 * @param string date format
 * @return array
 * @author Ali OYGUR <alioygur@gmail.com>
 */
function dateRange($first, $last, $step = '+1 day', $format = 'd/m/Y',$fix=FALSE ) {

    date_default_timezone_set("Europe/Helsinki");

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);


    while( $current <= $last ) {

        if ($fix) {
        	$dates[] = strftime($format, $current);
        } else {
        	$dates[] = date($format, $current);
        }
        $current = strtotime($step, $current);
    }

    return $dates;
}

// converts stdObject to array
function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}

		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}

// puts array/string to file and creates dirs
function file_force_contents($dir, $contents)
{
  $parts = explode('/', $dir);
  $file = array_pop($parts);
  $dir = '';
  foreach($parts as $part) {
    if(!is_dir($dir .= "$part/")) {
      mkdir($dir);
    }
  }
  file_put_contents("$dir/$file", $contents);
}

?>