<?php
require_once("wb_functions.php");

$missionid = 0;
$missionids = array();
while ($arg = array_shift($argv))
{
  switch ($arg) {
    case "-id" : {
		$multible = FALSE;
	    $missionid = array_shift($argv);
      	break;
    }
    case "-ids" : {
    	$multible = TRUE;
	    $missionids = explode(",",array_shift($argv));
      	break;
    }
    case "-debug" : {
	  	$api = "http://whambush.net/api/v2.0/";
      	break;
    }
  }
}

if ($missionid == 0 && $multible == FALSE) {
	echo "Missing id mission (-id # / -ids #,#,..)".PHP_EOL;
	exit;
}
if (count($missionids) < 2 && $multible == TRUE) {
	echo "Two or more ids needed (-ids #,#,#..)".PHP_EOL;
	exit;
}


function single_mission($missionid,$loginarray,&$data = NULL) {
	$missiondata = http_get("missions/$missionid/",$loginarray['token']);

	$missionvideospage = http_get("search/videos/?mission=$missionid",$loginarray['token']);

	$numofvideos = $missionvideospage['count'];
	
	$next = $missionvideospage['next'];
	$missionvideos =  $missionvideospage['results'];
	while ($next != "") {
		$missionvideospage = http_get($next,$loginarray['token'],TRUE);
		$missionvideos = array_merge($missionvideos, $missionvideospage['results']);
		$next = $missionvideospage['next'];
	}
	
	if ($missiondata['linked_video'] != "") {
		$missionlinkedvideo = $missiondata['linked_video'];
	}

	if ($missiondata['mission_type']) {
		$missiontype = "Icon";
	} else {
		$missiontype = "Normal";
	}

	echo PHP_EOL;
	echo $missiontype." mission: ".$missiondata['name']." (".$missiondata['country'].") ".PHP_EOL;
	echo PHP_EOL;

	if (isset($missionlinkedvideo)) {
		echo "Icon video:".PHP_EOL;
		echo "Views: ".$missionlinkedvideo['view_count'].PHP_EOL;
		echo "Activities: ";
		echo $missionlinkedvideo['comment_count']+$missionlinkedvideo['like_count']+$missionlinkedvideo['dislike_count'].PHP_EOL;
		if ($data != NULL) {
			$data['has_icon'] = TRUE;
			$data['icon_views']    += $missionlinkedvideo['view_count'];
			$data['icon_activity'] += $missionlinkedvideo['comment_count']+$missionlinkedvideo['like_count']+$missionlinkedvideo['dislike_count'];
		}
	}

	echo PHP_EOL;

	echo "User videos:".PHP_EOL;
	$totalviews = 0;
	$totalactivities = 0;
	foreach ($missionvideos as $singlevideo) {
		if (intval($singlevideo['rank']) > 0) {
			$totalviews += $singlevideo['view_count'];
			$totalactivities += $singlevideo['comment_count']+$singlevideo['like_count']+$singlevideo['dislike_count'];
		}  else {
			$numofvideos -= 1;
		}
	}
	echo "Number of videos: ".$numofvideos.PHP_EOL;
	echo "Number of views: ".$totalviews.PHP_EOL;
	echo "Number of activities: ".$totalactivities.PHP_EOL;
	if ($data != NULL) {
		$data['num_of_videos']   += $numofvideos;
		$data['num_of_views']    += $totalviews;
		$data['num_of_activity'] += $totalactivities;
	} 
	echo PHP_EOL;
}

$loginarray = login();
if ($multible) {
	$data = array('has_icon' => FALSE,'icon_views' => 0, 'icon_activity' => 0,'num_of_videos' => 0,'num_of_views' => 0,'num_of_activity' => 0);
	foreach ($missionids as $value) {
		single_mission($value,$loginarray,$data);
		echo "--".PHP_EOL;
	}
	echo "Total:".PHP_EOL;
	echo PHP_EOL;
	if ($data['has_icon']) {
		echo "Icon videos:".PHP_EOL;
		echo "Views: ".$data['icon_views'].PHP_EOL;
		echo "Activities: ".$data['icon_activity'].PHP_EOL;
		echo PHP_EOL;
	}
	echo "User videos:".PHP_EOL;
	echo "Number of videos: ".$data['num_of_videos'].PHP_EOL;
	echo "Number of views: ".$data['num_of_views'].PHP_EOL;
	echo "Number of activities: ".$data['num_of_activity'].PHP_EOL;
	//print_r($data);
} else {
	single_mission($missionid,$loginarray);	
}

echo PHP_EOL."done".PHP_EOL;

?>