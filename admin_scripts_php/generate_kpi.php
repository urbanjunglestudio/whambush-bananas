<?php
//input file columns
define("kACTION",0);
define("kUSERID",1);
define("kTARGETID",2);
define("kDATE",3);
define("kAPPVERSION",4);
define("kDEVICE",5);
define("kMODEL",6);
define("kOS",7);
define("kOSVERSION",8);

require_once("wb_functions.php");

$debug = FALSE;
$page_size = 100;
$input_file = "";
$force = FALSE;
$start_date = "1.4.2014";
$end_date = "yesterday";

date_default_timezone_set("Europe/Helsinki");
$now = date("Hi_d.m.Y");
$today = date("d.m.Y");
$file = "all_data_".$now.".csv";

$output_dir = "./data_".$today;

while ($arg = array_shift($argv))
{
	switch ($arg) {
		case "-debug" : {
			$debug = TRUE;
    	$page_size = 10;
			break;
		}
		case "-i" : {
			$input_file = array_shift($argv);
			break;
		}
		case "-force" : {
			$force = TRUE;
			break;
		}
		case "-o" : {
			$output_dir = array_shift($argv);
			break;
		}
		case "-sd" : {
		  $start_date = array_shift($argv);
		  break;
		}
		case "-ed" : {
		  $end_date = array_shift($argv);
		  break;
		}
	}
}

if (!isset($output_dir)) {
  $output_dir = "./data_".$start_date."-".$end_date;
}

if ($force) {
	echo "Generating new input file (option -i ignored)!".PHP_EOL;
	require("create_activity_data_file.php");
	$input_file = $file;
}

if (strlen($input_file) < 1) {
	echo "ERROR: input file (-i) missing".PHP_EOL;
	exit;
} else {
	echo "Inputfile: ".$input_file.PHP_EOL;
}

$file = file($input_file);

$dates = dateRange($start_date." -1 second",$end_date,"+1 day","d M Y");
for ($i = 0; $i < count($dates); $i++) {
	$dailylogins[$dates[$i]] = 0;
	$dailyindividuallogins[$dates[$i]] = array();
	$dailylikes[$dates[$i]] = 0;
	$dailydislikes[$dates[$i]] = 0;
	$dailycomments[$dates[$i]] = 0;
	$dailyvideos[$dates[$i]] = 0;
}

$weeks = dateRange($start_date." -1 second",$end_date,"+1 week","%V/%G",TRUE);
for ($i = 0; $i < count($weeks); $i++) {
	$weeklylogins[$weeks[$i]] = 0;
	$weeklyindividuallogins[$weeks[$i]] = array();
	$weeklylikes[$weeks[$i]] = 0;
	$weeklydislikes[$weeks[$i]] = 0;
	$weeklycomments[$weeks[$i]] = 0;
	$weeklyvideos[$weeks[$i]] = 0;
}

$months = dateRange($start_date." -1 second",$end_date,"+1 month","M/Y",FALSE);
for ($i = 0; $i < count($months); $i++) {
	$monthlylogins[$months[$i]] = 0;
	$monthlyindividuallogins[$months[$i]] = array();
	$monthlylikes[$months[$i]] = 0;
	$monthlydislikes[$months[$i]] = 0;
	$monthlycomments[$months[$i]] = 0;
	$monthlyvideos[$months[$i]] = 0;
}

$totallogins = 0;

foreach ($file as $line) {
  $linearray = explode(",",$line);
  $time = strtotime($linearray[kDATE]);
  if ($time > strtotime($start_date) && $time < strtotime($end_date)) {
    $date = date("d M Y",$time);
    $week = strftime("%V/%G",$time);
    $month = date("M/Y",$time);
   	if ($linearray[kACTION] == "logged in") {
      $dailylogins[$date] = $dailylogins[$date] + 1;
      $dailyindividuallogins[$date][$linearray[kUSERID]] =  $dailyindividuallogins[$date][$linearray[kUSERID]] + 1;
      $dailyuserlogins[$linearray[kUSERID]][$date] =  $dailyuserlogins[$linearray[kUSERID]][$date] + 1;

      $weeklylogins[$week] = $weeklylogins[$week] + 1;
      $weeklyindividuallogins[$week][$linearray[kUSERID]] =  $weeklyindividuallogins[$week][$linearray[kUSERID]] + 1;
      $weeklyuserlogins[$linearray[kUSERID]][$week] =  $weeklyuserlogins[$linearray[kUSERID]][$week] + 1;
      
      $monthlylogins[$month] = $monthlylogins[$month] + 1;
      $monthlyindividuallogins[$month][$linearray[kUSERID]] =  $monthlyindividuallogins[$month][$linearray[kUSERID]] + 1;
      $monthlyuserlogins[$linearray[kUSERID]][$month] =  $monthlyuserlogins[$linearray[kUSERID]][$month] + 1;

      $totallogins = $totallogins + 1;
    }
    if ($linearray[kACTION] == "liked") {
      $dailylikes[$date] = $dailylikes[$date] + 1;
      $weeklylikes[$week] = $weeklylikes[$week] + 1;
      $monthlylikes[$month] = $monthlylikes[$month] + 1;
    }
    if ($linearray[kACTION] == "disliked") {
      $dailydislikes[$date] = $dailydislikes[$date] + 1;
      $weeklydislikes[$week] = $weeklydislikes[$week] + 1;
      $monthlydislikes[$month] = $monthlydislikes[$month] + 1;
    }
    if ($linearray[kACTION] == "commented on") {
  	  $dailycomments[$date] = $dailycomments[$date] + 1;
      $weeklycomments[$week] = $weeklycomments[$week] + 1;
      $monthlycomments[$month] = $monthlycomments[$month] + 1;
    }
    if ($linearray[kACTION] == "uploaded") {
  	  $dailyvideos[$date] = $dailyvideos[$date] + 1;
  	  $weeklyvideos[$week] = $weeklyvideos[$week] + 1;
  	  $monthlyvideos[$month] = $monthlyvideos[$month] + 1;
    }
  }
}

echo PHP_EOL."Outputs: ".PHP_EOL;

//////////////////////////////
$to_file = "date,total logins,users logged in".PHP_EOL;
foreach ($dailylogins as $key => $logins) {
	$to_file .= $key.",".$logins.",".count($dailyindividuallogins[$key]).PHP_EOL;
}
echo "-> logins per day: ".$output_dir."/all_logins_daily.csv".PHP_EOL;
file_force_contents($output_dir."/all_logins_daily.csv", $to_file);

//////////////////////////////
$to_file = "week,total logins,users logged in".PHP_EOL;
foreach ($weeklylogins as $key => $logins) {
	$to_file .= $key.",".$logins.",".count($weeklyindividuallogins[$key]).PHP_EOL;
}
echo "-> logins per week: ".$output_dir."/all_logins_weekly.csv".PHP_EOL;
file_force_contents($output_dir."/all_logins_weekly.csv", $to_file);

//////////////////////////////
$to_file = "month,total logins,users logged in".PHP_EOL;
foreach ($monthlylogins as $key => $logins) {
	$to_file .= $key.",".$logins.",".count($monthlyindividuallogins[$key]).PHP_EOL;
}
echo "-> logins per month: ".$output_dir."/all_logins_monthly.csv".PHP_EOL;
file_force_contents($output_dir."/all_logins_monthly.csv", $to_file);

//////////////////////////////
$to_file = "date,likes,dislikes,comments,videos".PHP_EOL;
foreach ($dailylikes as $key => $likes) {
	$to_file .= $key.",".$likes.",".$dailydislikes[$key].",".$dailycomments[$key].",".$dailyvideos[$key].PHP_EOL;
}
echo "-> actions per day: ".$output_dir."/user_activity_daily.csv".PHP_EOL;
file_force_contents($output_dir."/user_activity_daily.csv", $to_file);

//////////////////////////////
$to_file = "week,likes,dislikes,comments,videos".PHP_EOL;
foreach ($weeklylikes as $key => $likes) {
	$to_file .= $key.",".$likes.",".$weeklydislikes[$key].",".$weeklycomments[$key].",".$weeklyvideos[$key].PHP_EOL;
}
echo "-> actions per week: ".$output_dir."/user_activity_weekly.csv".PHP_EOL;
file_force_contents($output_dir."/user_activity_weekly.csv", $to_file);

//////////////////////////////
$to_file = "month,likes,dislikes,comments,videos".PHP_EOL;
foreach ($monthlylikes as $key => $likes) {
	$to_file .= $key.",".$likes.",".$monthlydislikes[$key].",".$monthlycomments[$key].",".$monthlyvideos[$key].PHP_EOL;
}
echo "-> actions per month: ".$output_dir."/user_activity_monthly.csv".PHP_EOL;
file_force_contents($output_dir."/user_activity_monthly.csv", $to_file);

//////////////////////////////
$to_file = "user";
foreach ($dailylogins as $date => $null) {
	$to_file .= ",".$date;
}
$to_file .= PHP_EOL;
foreach ($dailyuserlogins as $user => $userdata) {
	$to_file .= $user;
	foreach ($dailylogins as $date => $null) {
		if (isset($userdata[$date])) {
  		$to_file .= ",".$userdata[$date];
		} else {
  		$to_file .= ",0";
		}
	}
	$to_file .= PHP_EOL;
}
echo "-> number of user logins per day: ".$output_dir."/user_logins_daily.csv".PHP_EOL;
file_force_contents($output_dir."/user_logins_daily.csv", $to_file);

//////////////////////////////
$to_file = "user";
foreach ($weeklylogins as $week => $null) {
	$to_file .= ",".$week;
}
$to_file .= PHP_EOL;
ksort($weeklyuserlogins);
foreach ($weeklyuserlogins as $user => $userdata) {
	$to_file .= $user;
	foreach ($weeklylogins as $week => $null) {
		if (isset($userdata[$week])) {
  		$to_file .= ",".$userdata[$week];
		} else {
  		$to_file .= ",0";
		}
	}
	$to_file .= PHP_EOL;
}
echo "-> number of user logins per week: ".$output_dir."/user_logins_weekly.csv".PHP_EOL;
file_force_contents($output_dir."/user_logins_weekly.csv", $to_file);

//////////////////////////////
$to_file = "user";
foreach ($monthlylogins as $month => $null) {
	$to_file .= ",".$month;
}
$to_file .= PHP_EOL;
ksort($monthlyuserlogins);
foreach ($monthlyuserlogins as $user => $userdata) {
	$to_file .= $user;
	foreach ($monthlylogins as $month => $null) {
		if (isset($userdata[$month])) {
  		$to_file .= ",".$userdata[$month];
		} else {
  		$to_file .= ",0";
		}
	}
	$to_file .= PHP_EOL;
}
echo "-> number of user logins per month: ".$output_dir."/user_logins_monthly.csv".PHP_EOL;
file_force_contents($output_dir."/user_logins_monthly.csv", $to_file);

//////////////////////////////
$to_file = "user,logins".PHP_EOL;
ksort($dailyuserlogins);
foreach ($dailyuserlogins as $user => $userdata) {
	$to_file .= $user.",".count($userdata).PHP_EOL;
	$userdaylogins[count($userdata)][$user] = "";
}
echo "-> number of logins per user per day: ".$output_dir."/number_of_logins_daily.csv".PHP_EOL;
file_force_contents($output_dir."/number_of_logins_daily.csv", $to_file);

//////////////////////////////
ksort($userdaylogins);
$to_file = "days,users".PHP_EOL;
foreach ($userdaylogins as $days => $users) {
	$to_file .= $days.",".count($users).PHP_EOL;
}
echo "-> number of logins per day per user: ".$output_dir."/number_of_user_logins_daily.csv".PHP_EOL;
file_force_contents($output_dir."/number_of_user_logins_daily.csv", $to_file);

/////////////////////////////
echo PHP_EOL;
echo "total logins:".PHP_EOL;
echo $totallogins.PHP_EOL;
echo "user who logged:".PHP_EOL;
echo count($dailyuserlogins).PHP_EOL;


?>
