<?php
require_once("wb_functions.php");

$printStats = FALSE;
$debug = FALSE;
$page_size = 100;

foreach ($argv as $arg) {
  if  ($arg == "-stats") {
    $printStast = TRUE;
  }
//  if  ($arg == "-append") {
//    $append = TRUE;
//  }
  if  ($arg == "-debug") {
    $debug = TRUE;
    $page_size = 10;
  }
}

////////////////

if (!$append) {
	$loginarray = login();
}

//normal + mission videos
echo "normal+mission videos".PHP_EOL;

$videoarray = http_get("videos/?&page_size=".$page_size,$loginarray['token']);

$totalcount = $videoarray['count'];

$next = $videoarray['next'];
$allresults =  $videoarray['results'];

if (!$append) {
  echo "Videos: ".count($allresults)."/".$totalcount.PHP_EOL;
}
while ($next != "") {
  $videoarray = http_get($next,$loginarray['token'],TRUE);
  $allresults = array_merge($allresults, $videoarray['results']);
  $next = $videoarray['next'];
  if ($debug) {
    if (count($allresults) > $page_size * 2) {
      break;
    }
  }
  if (!$append) {
    echo "Videos: ".count($allresults)."/".$totalcount.PHP_EOL;
  }
}

//tv videos
echo "tv videos".PHP_EOL;

$tvarray = http_get("videos/?&page_size=".$page_size,$loginarray['token']);

$totalcount += $tvarray['count'];

$next = $tvarray['next'];
$allresults =  array_merge($allresults, $tvarray['results']);

if (!$append) {
  echo "TV Videos: ".count($allresults)."/".$totalcount.PHP_EOL;
}
while ($next != "") {
  $tvarray = http_get($next,$loginarray['token'],TRUE);
  $allresults = array_merge($allresults, $tvarray['results']);
  $next = $videoarray['next'];
  if ($debug) {
    if (count($allresults) > $page_size * 2) {
      break;
    }
  }
  if (!$append) {
    echo "Videos: ".count($allresults)."/".$totalcount.PHP_EOL;
  }
}

$result = array();
$mostviewsvideo = array();
$mostbananasvideo = array();
$mostshitvideo = array();
$mostbananstouservideo = array();
$totalvideoviews = 0;

foreach ($allresults as $singlevideo) {
    $user = $singlevideo['added_by']['username'];
    if (!isset($result[$user]['email'])) {
      $result[$user]['email'] = $singlevideo['added_by']['email'];;
    }
    if (!isset($result[$user]['video'])) {
      $result[$user]['video'] = 1;
    } else {
      $result[$user]['video'] += 1;
    }
    if (!isset($result[$user]['bananas'])) {
      $result[$user]['bananas'] = $singlevideo['added_by']['userscore'];;
    }
    $totalvideoviews += $singlevideo['view_count'];
    $time = strtotime($singlevideo['created_at']);
	  $month = date("M",$time);

    
    $mostviewsvideo[$singlevideo['web_url']] = $singlevideo['view_count'];
    $mostbananasvideo[$singlevideo['web_url']] = $singlevideo['like_count'];
    $mostshitvideo[$singlevideo['web_url']] = $singlevideo['dislike_count'];
    $mostbananstouservideo[$singlevideo['web_url']] = $singlevideo['like_count'] - $singlevideo['dislike_count'];
}

date_default_timezone_set("Europe/Helsinki");

if (!$printStast) {
  $index = 1;
  $tofile[0] = "username\temail\tvideos\tbananas\tbananas/videos".PHP_EOL;
  foreach ($result as $key => $singleuser) {
    $bdivv = number_format(round(intval($singleuser['bananas'])/intval($singleuser['video']),1),1,",","");
    $tofile[$index] = $singleuser['email']."\t".$key."\t".$singleuser['video']."\t".$singleuser['bananas']."\t".$bdivv.PHP_EOL;
    $index += 1;
  }
  date_default_timezone_set("Europe/Helsinki");
  $now = date("_Hi_d.m.Y");
  file_put_contents("./user_video_list".$now."_tab_separated.csv", implode($tofile));
} else {
  $now = date("d.m \k\l\o H.i");
  $moreThanOne = 0;
  $videoArray = array();
  $bananaArray = array();
  foreach ($result as $key => $singleuser) {
    if (intval($singleuser['video']) > 1) {
      $moreThanOne++;
    }
    $videoArray[$key] = $singleuser['video'];
    $bananaArray[$key] = $singleuser['bananas'];
  }
  arsort($videoArray);
  $smallVideo = array_slice($videoArray,0,10);
  arsort($bananaArray);
  $smallBanana = array_slice($bananaArray,0,10);
  if (!$append) {
  	echo PHP_EOL."--------------".PHP_EOL."Stats ".$now.PHP_EOL;
  	echo PHP_EOL."users (total):".PHP_EOL."XXX".PHP_EOL;
  }
  echo PHP_EOL."videos (total):".PHP_EOL.count($allresults).PHP_EOL;
  echo PHP_EOL."videos viewed (total):".PHP_EOL.$totalvideoviews.PHP_EOL;

 
  echo PHP_EOL."users who have uploaded a video:".PHP_EOL.count($videoArray).PHP_EOL;
  echo PHP_EOL."users who have uploaded more than one video:".PHP_EOL.$moreThanOne.PHP_EOL;

  echo PHP_EOL."most videos (user):".PHP_EOL."--------------".PHP_EOL;
  foreach ($smallVideo as $key => $data) {
    echo $key." ".$data.PHP_EOL;
  }
  echo PHP_EOL."most bananas (user):".PHP_EOL."--------------".PHP_EOL;
  foreach ($smallBanana as $key => $data) {
    echo $key." ".$data.PHP_EOL;
  }
  arsort($mostviewsvideo);
  $smallMostviewsvideo = array_slice($mostviewsvideo,0,10);
  echo PHP_EOL."most views (video):".PHP_EOL."--------------".PHP_EOL;
  foreach ($smallMostviewsvideo as $key => $data) {
    echo $key." ".$data.PHP_EOL;
  }
  arsort($mostbananasvideo);
  $smallMostbananasvideo = array_slice($mostbananasvideo,0,10);
  echo PHP_EOL."most bananas (video):".PHP_EOL."--------------".PHP_EOL;
  foreach ($smallMostbananasvideo as $key => $data) {
    echo $key." ".$data.PHP_EOL;
  }
  arsort($mostshitvideo);
  $smallMostshitvideo = array_slice($mostshitvideo,0,10);
  echo PHP_EOL."most shit (video):".PHP_EOL."--------------".PHP_EOL;
  foreach ($smallMostshitvideo as $key => $data) {
    echo $key." ".$data.PHP_EOL;
  }
  arsort($mostbananstouservideo);
  $smallMostbananstouservideo = array_slice($mostbananstouservideo,0,10);
  echo PHP_EOL."most bananas to user (video):".PHP_EOL."--------------".PHP_EOL;
  foreach ($smallMostbananstouservideo as $key => $data) {
    echo $key." ".$data.PHP_EOL;
  }
  //echo PHP_EOL."--------------".PHP_EOL;
}
if (!$append) {
  echo PHP_EOL."done".PHP_EOL;
}
?>