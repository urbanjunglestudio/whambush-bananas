<?php
require_once("wb_functions.php");

date_default_timezone_set("Europe/Helsinki");

$debug = FALSE;
$page_size = 100;
$notv = FALSE;
$noicon = FALSE;
$download = FALSE;
$link = FALSE;

while ($arg = array_shift($argv))
{
	switch ($arg) {
		case "-debug" : {
			$debug = TRUE;
    	$page_size = 10;
			break;
		}
		case "-noicon" : {
			$noicon = TRUE;
			break;
		}
		case "-notv" : {
			$notv = TRUE;
			break;
		}
		case "-download" : {
			$download = TRUE;
			break;
		}
		case "-link" : {
			$link = TRUE;
			break;
		}
		
	}
}

if(!($download || $link)) {
	echo "Select either -link and/or -download option!".PHP_EOL;
	exit;
}

$loginarray = login();

if (!$noicon) {
	echo PHP_EOL."/////////// icon videos //////////".PHP_EOL;

	$activemissionsarray = http_get("missions/?type=old&page_size=".$page_size,$loginarray['token']);

	$oldmissionsarray = http_get("missions/?type=old&page_size=".$page_size,$loginarray['token']);

	$next = $oldmissionsarray['next'];
	$allresults =  array_merge($oldmissionsarray['results'], $activemissionsarray['results']);


	while ($next != "") {
		$oldmissionsarray = http_get($next,$loginarray['token'],TRUE);
		$allresults = array_merge($allresults, $oldmissionsarray['results']);
		$next = $oldmissionsarray['next'];
		if ($debug) {
			if (count($allresults) > $page_size) {
				break;
			}
		}
	}

	foreach ($allresults as $singlemission) {
		if ($singlemission['mission_type'] == 1) {
			$date = date("d M Y",strtotime($singlemission['linked_video']['published_at']));
			if ($download) {
				echo $date." - ".$singlemission['linked_video']['name']." - "."http://view.vzaar.com/".$singlemission['linked_video']['external_id']."/download".PHP_EOL;
			}
			if ($link) {
				echo $date." - ".$singlemission['linked_video']['name']." - ".$singlemission['linked_video']['web_url'].PHP_EOL;
			}
		}
	}
}

if (!$notv) {
	echo PHP_EOL."/////////// WhambushTV videos //////////".PHP_EOL;

	$tvarray = http_get("videos/?type=tv&page_size=".$page_size,$loginarray['token']);
	$next = $tvarray['next'];

	$allresults =  $tvarray['results'];

	while ($next != "") {
		$tvarray = http_get($next,$loginarray['token'],TRUE);
		$allresults = array_merge($allresults, $tvarray['results']);
		$next = $tvarray['next'];
		if ($debug) {
			if (count($allresults) > $page_size) {
				break;
			}
		}
	}

	foreach ($allresults as $tv) {
		$date = date("d M Y",strtotime($tv['published_at']));
		if ($download) {
			echo $date." - ".$tv['name']." - "."http://view.vzaar.com/".$tv['external_id']."/download".PHP_EOL;
		}
		if ($link) {
			echo $date." - ".$tv['name']." - ".$tv['web_url'].PHP_EOL;
		}
	}
}

echo PHP_EOL;

?>
