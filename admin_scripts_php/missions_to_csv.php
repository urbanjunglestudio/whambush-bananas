<?php
require_once("wb_functions.php");

$debug = FALSE;
$page_size = 100;

foreach ($argv as $arg) {
  if  ($arg == "-debug") {
    $debug = TRUE;
    $page_size = 10;
  }
}

$loginarray = login();


$missionsarray = http_get("missions/?type=old&page_size=".$page_size,$loginarray['token']);

$totalcount = $missionsarray['count'];

$next = $missionsarray['next'];
$allresults =  $missionsarray['results'];

echo "Got: ".count($allresults)."/".$totalcount.PHP_EOL;

while ($next != "") {
	$missionsarray = http_get($next,$loginarray['token'],TRUE);
	$allresults = array_merge($allresults, $missionsarray['results']);
	$next = $missionsarray['next'];
	if ($debug) {
		if (count($allresults) > $page_size * 2) {
			break;
		}
	}
	echo "Got: ".count($allresults)."/".$totalcount.PHP_EOL;
}
echo PHP_EOL."/////////////////////".PHP_EOL;

$totalvideos = 0;

$tofile = "";

foreach ($allresults as $singlemission) {
  $lbs   = array("\r\n", "\n", "\r");
  $desc = str_replace($lbs, " ", $singlemission['description']);

  $tofile .= $singlemission['name']."\t".PHP_EOL;
  $tofile .= $desc."\t".PHP_EOL;
  $tofile .= ""."\t".PHP_EOL;

}
file_force_contents("./all_missions.csv", $tofile);

echo PHP_EOL."Done".PHP_EOL;




















?>