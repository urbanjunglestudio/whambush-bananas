<?php
require_once("wb_functions.php");

date_default_timezone_set("Europe/Helsinki");
$now = date("d.m.Y_Hi");
$file = "all_data_".$now.".csv";

if (!isset($debug)) {
	$debug = FALSE;
	$page_size = 100;
  $start_date = "1.4.2014";
	$end_date = "today";

  while ($arg = array_shift($argv))
  {
    switch ($arg) {
      case "-debug" : {
        $debug = TRUE;
        $page_size = 10;
        break;
      }
      case "-sd" : {
        $start_date = array_shift($argv);
        break;
      }
      case "-ed" : {
        $end_date = array_shift($argv);
        break;
      }
    }
  }
}


function putActivityDataToFile($data,$new=FALSE)
{
  global $file;
  global $start_date;
  global $end_date;

  $to_file = "";

  $ignore = FALSE;
  $data_string = "";

  foreach ($data as $single) {

    if (strtotime($single['created_at']) > strtotime($end_date)) {    // ignore data after end date //TODO:FIX ME
    	$ignore = TRUE;
    }

    if (date("d M Y",strtotime($start_date."-1 second")) == date("d M Y",strtotime($single['created_at']))) {    // ignore data before start date
    	$ignore = TRUE;
    	echo "Start date met".PHP_EOL;
    	exit;
    }

    if ($single['verb'] == "logged in" || !$ignore) {
      $loginData = objectToArray(json_decode($single['data']));
      if($loginData['device'] == "admin" || $single['actor_object_id'] == "9314") {       //ignore admin logins
        $ignore = TRUE;
      }
      $data_string = $loginData['whambush_version'].",".$loginData['manufacturer'].",".str_replace(",",".",$loginData['device']).",".$loginData['os'].",".$loginData['os_version'];
    }

    if (!$ignore) {
      $to_file .= $single['verb'].",".$single['actor_object_id'].",".$single['target_object_id'].",".$single['created_at'].",".$data_string.PHP_EOL;
    }

    $ignore = FALSE;
    $data_string = "";

  }

  if ($new) {
    file_put_contents($file, $to_file);
  } else {
    file_put_contents($file, $to_file, FILE_APPEND);
  }
}


$loginarray = login();

$alldatasarray = http_get("kpi/?page_size=".$page_size,$loginarray['token']);

$gotcount = 0;
$totalcount = $alldatasarray['count'];

if	($alldatasarray['count'] < 1 ) {
  print_r($alldatasarray['count']);
	echo PHP_EOL."- You need to be an admin to use this script!".PHP_EOL;
	exit;
}

$next = $alldatasarray['next'];
$gotcount = count($alldatasarray['results']);
echo "Data: ".$gotcount."/".$totalcount.PHP_EOL;
putActivityDataToFile($alldatasarray['results'],TRUE);

while ($next != "") {
	$alldatasarray = http_get($next,$loginarray['token'],TRUE);

	$next = $alldatasarray['next'];
  $gotcount = $gotcount + count($alldatasarray['results']);
	echo "Data: ".$gotcount."/".$totalcount.PHP_EOL;
  putActivityDataToFile($alldatasarray['results']);

	if ($debug) {
		if ($gotcount > $page_size * 1) {
			break;
		}
	}
}

echo PHP_EOL."done".PHP_EOL;

?>