<?php
require_once("wb_functions.php");

$debug = FALSE;
$stats = FALSE;
$page_size = 100;

foreach ($argv as $arg) {
  if  ($arg == "-debug") {
    $debug = TRUE;
    $page_size = 10;
  }
  if  ($arg == "-stats") {
    $stats = TRUE;
  }
}


$loginarray = login();

$allusersarray = http_get("users/?page_size=".$page_size,$loginarray['token']);

$totalcount = $allusersarray['count'];

if	($allusersarray['count'] < 1 ) {
  print_r($allusersarray['count']);
	echo PHP_EOL."- You need to be an admin to use this script!".PHP_EOL;
	exit;
}

$next = $allusersarray['next'];
$allresults =  $allusersarray['results'];

echo "Users: ".count($allresults)."/".$totalcount.PHP_EOL;

while ($next != "") {
	$allusersarray = http_get($next,$loginarray['token'],TRUE);
	$allresults = array_merge($allresults, $allusersarray['results']);
	$next = $allusersarray['next'];
	if ($debug) {
		if (count($allresults) > $page_size * 2) {
			break;
		}
	}
	echo "Users: ".count($allresults)."/".$totalcount.PHP_EOL;
}

$activeusers = 0;  // activation_state
$inactiveusers = 0;  // activation_state
$bannedusers = 0;  // activation_state
$newusers = 0;  // activation_state
$guests = 0;  // is_guest
$userspermonth = array();
$usersbirthyears = array();

$dates = dateRange("1.4.2014","today","+1 day","d M Y");
$usersperday = array();  // date_joined
for ($i = 0; $i < count($dates); $i++) {
  $usersperday[$dates[$i]] = 0;
}

$followers = array();  // num_followers
$followings = array();  // num_followings

date_default_timezone_set("Europe/Helsinki");

foreach ($allresults as $singleuser) {
  if	($debug) {
  	print_r($singleuser);
  }
  if ($singleuser['activation_state'] == "ACTIVATED") {
    $activeusers++;
  }
  if ($singleuser['activation_state'] == "INACTIVE") {
    $inactiveusers++;
  }
  if ($singleuser['activation_state'] == "DEACTIVATED") {
    $bannedusers++;
  }
  if ($singleuser['activation_state'] == "NEW") {
    $newusers++;
  }
  if ($singleuser['is_guest'] == 1) {
    $guests++;
  }
  $time = strtotime($singleuser['date_joined']);
  $bday = strtotime($singleuser['birthday']);
  $date = date("d M Y",$time);
  $month = date("M",$time);
  $byear = date("Y",$bday);
  $usersbirthyears[$byear] = $usersbirthyears[$byear] + 1;
  $usersperday[$date] =  $usersperday[$date] + 1;
  $userspermonth[$month] =  $userspermonth[$month] + 1;
  $followers[$singleuser['username']] = $singleuser['num_followers'];
  $followings[$singleuser['username']] = $singleuser['num_followings'];

  $emails[$singleuser['username']] = $singleuser['email'];
}
echo PHP_EOL;
$now = date("d.m \k\l\o H.i");
echo PHP_EOL."--------------".PHP_EOL."Stats ".$now.PHP_EOL;

echo PHP_EOL;
echo "total registered users: "; print_r(count($allresults));
echo PHP_EOL;
//if (!$stats) {
  echo "active users: "; print_r($activeusers);
  echo PHP_EOL;
  echo "inactive users: "; print_r($inactiveusers-$guests);
  echo PHP_EOL;
  echo "banned users: "; print_r($bannedusers);
  echo PHP_EOL;
  echo "new users: "; print_r($newusers);
  echo PHP_EOL;
//}
echo "guests: "; print_r($guests);
echo PHP_EOL;

echo PHP_EOL."new users per month:".PHP_EOL;
foreach ($userspermonth as $key => $value)  {
  echo $key.": ".$value.PHP_EOL;
}
echo PHP_EOL."users birth years:".PHP_EOL;
ksort($usersbirthyears);
foreach ($usersbirthyears as $key => $value)  {
  echo $key.": ".$value.PHP_EOL;
}

if ($stats) {
  $append = TRUE;
  include("create_user_video_list.php");
}

arsort($followers);
$smallFollowers = array_slice($followers,0,10);
echo PHP_EOL."most followers (user):".PHP_EOL."--------------".PHP_EOL;
foreach ($smallFollowers as $key => $value)  {
  echo $key." ".$value.PHP_EOL;
}
arsort($followings);
$smallFollowings = array_slice($followings,0,10);
echo PHP_EOL."most followings (user):".PHP_EOL."--------------".PHP_EOL;
foreach ($smallFollowings as $key => $value)  {
  echo $key." ".$value.PHP_EOL;
}

$cumulative = 0;
$tofile = "";
foreach ($usersperday as $key => $value) {
  $cumulative += $value;
  $tofile .= $key.",".$value.",".$cumulative.PHP_EOL;
}
file_put_contents("./all_users_per_day.csv",$tofile);
//print_r($tofile);

$tofile = "";
foreach ($emails as $key => $value) {
	if (strlen(strstr($value,"@")) > 1) {
		$tofile .= $value.PHP_EOL;
	}
}
file_force_contents("./all_emails.txt",$tofile);


echo PHP_EOL."done".PHP_EOL;

?>