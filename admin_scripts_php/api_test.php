<?php
require_once("wb_functions.php");

$page_size = "page_size=10";

while ($arg = array_shift($argv))
{
  switch ($arg) {
    case "-page_size" : {
	    $page_size = "page_size=".array_shift($argv);
      break;
    }
    case "-api" : {
	    $apipath = array_shift($argv);
      break;
    }
    case "-debug" : {
	    $api = "https://whambush.com/api/v2.1/";
      break;
    }
  }
}

$loginarray = login();

if (isset($apipath)) {
  if ($apipath == "login/") {
    print_r($loginarray);
  } else {
    if (strpos($apipath, "?") === false) {
      echo "?";
      $separator = "?";
    } else {
      echo "&";
      $separator = "&";
    }
  	$answer = http_get($apipath.$separator.$page_size,$loginarray['token']);
  	print_r($answer);
  }
} else {
	echo "-> use -api <api> to define which to test".PHP_EOL;
}

////////////////

echo PHP_EOL."done".PHP_EOL;
?>
