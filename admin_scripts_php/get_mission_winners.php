<?php
require_once("wb_functions.php");

$icon_videos = 10;
$mission_videos = 3;
$num_of_missions = 7;
$page_size = 100;
$nomail = false;
$dl = false;
$country = "FI";

while ($arg = array_shift($argv))
{
	switch ($arg) {
		case "-debug" : {
			$debug = TRUE;
    		$page_size = 10;
			break;
		}
		case "-iv" : {
 			$icon_videos = array_shift($argv);
			break;
		}
		case "-mv" : {
			$mission_videos = array_shift($argv);
			break;
		}
		case "-nm" : {
			$num_of_missions = array_shift($argv);
			break;
		}
		case "-ps" : {
			$page_size = array_shift($argv);
			break;
		}
		case "-nomail" : {
			$nomail = true;
			break;
		}
		case "-download" : {
			$dl = true;
			break;
		}
		case "-c" : {
			$country = array_shift($argv);
			break;
		}
		case "-h" : {
			echo "---------".PHP_EOL;
			echo "-iv #     : number of videos for icon mission (default 10)".PHP_EOL;
			echo "-mv #     : number of videos for normal mission (default 3)".PHP_EOL;
			echo "-nm #     : number of missions to be printed out(default 7)".PHP_EOL;
			echo "-ps #     : page_size for API (default 100)".PHP_EOL;
			echo "-nomail   : email addresses are not printed".PHP_EOL;
			echo "-download : print donwload links ".PHP_EOL;
			echo "---------".PHP_EOL;
			exit;
			break;
		}
	}
}

$loginarray = login();

echo "- ".$mission_videos." ".$icon_videos." ".$num_of_missions.PHP_EOL;

$missionsarray = http_get("missions/?country=".$country."&type=old&page_size=".$num_of_missions,$loginarray['token']);

//print_r($missionsarray);

foreach ($missionsarray['results'] as &$singlemission) {
  //echo $singlemission['mission_type'].PHP_EOL;
  if ($singlemission['mission_type'] == 1) {
    if ($icon_videos > 0) {
    	echo PHP_EOL."ICON: ".$singlemission['name'].PHP_EOL;
    }
    $maxcount = $icon_videos;
  } else {
  	if ($mission_videos > 0) {
  		echo PHP_EOL."NORM: ".$singlemission['name'].PHP_EOL;
  	}
  	$maxcount = $mission_videos;
  }
  $missionvideos = http_get("search/videos/?mission=".$singlemission['id']."&page_size=".$page_size,$loginarray['token']);
  if ($missionvideos['count'] > 0) {
    if ($missionvideos['count'] > $maxcount) {
      $count = $maxcount;
    } else {
      $count = $missionvideos['count'];
    }

    for ($i = 0; $i < $count; $i++) {
      $rank = $i + 1;
      $download_link = "";
      if ($dl) {
      	$download_link = ", "."http://view.vzaar.com/".$missionvideos['results'][$i]['external_id']."/download";
      }
      //print_r($missionvideos['results'][$i]);
    	if ($nomail) {
      		echo $rank.". ".$missionvideos['results'][$i]['added_by']['username'].", ".$missionvideos['results'][$i]['web_url'].$download_link.PHP_EOL;
    	} else {
    		echo $rank.". ".$missionvideos['results'][$i]['added_by']['username'].", ".$missionvideos['results'][$i]['added_by']['id'].", ".$missionvideos['results'][$i]['added_by']['email'].", ".$missionvideos['results'][$i]['web_url'].", ".$missionvideos['results'][$i]['added_by']['activation_state'].$download_link.$missionvideos['results'][$i]['bananas'].PHP_EOL;
    	}
    }
  } else {
    echo "-".PHP_EOL;
  }
}

unset($singlemission);

echo PHP_EOL."1".PHP_EOL;
?>