--[[
Whambush API library
(c) Urban Jungle Studios Oy / Jari Kalinainen
]]--

local help = require "_modules.helpers" 

local Whambush = 
{
	token = nil,
	username = nil,
	password = nil,
	api_version = "v1.3",
	api = nil,
	
	page_size = 10,
	
	--endpoints
	LOGIN = "login/",
	USERS = "users/",

	endpoint = nil,

	userTable = {}
}

function Whambush:authenticate()
	
	local body = {}
	body.username = self.username
	body.password = self.password
	
	assert( self.username ~= nil, "Error: No username" )
	assert( self.password ~= nil, "Error: No password" )
	
	local headers = {}
	
 	local answer = coronium.network.postJson(self.endpoint .. self.LOGIN,body,headers)
 	--help:print_all(answer)
 	--help:print_all(coronium.json.decode(answer.result.body))

	self.token = coronium.json.decode(answer.result.body).token
	--help:print_all( self.token )

end

function Whambush:getUser( userId , _callback )
	-- body
	assert( self.token , "Error: No auth token!" )
	local headers = {}
	headers["Content-Type"] = "application/json"
 	headers["Authorization"] = "Token " .. self.token

	local params = {}
	params.headers = headers
	
	local answer = coronium.network.getJson( self.endpoint .. self.USERS .. userId .. "/", "", headers )

	_callback(answer)
	
end

function Whambush:getAllUsers( _callback )
	-- body
	assert( self.token , "Error: No auth token!" )
	
	local headers = {}
	headers["Content-Type"] = "application/json"
 	headers["Authorization"] = "Token " .. self.token

	local params = {}
	params.headers = headers
	
	local answer = coronium.network.getJson( self.endpoint .. self.USERS .. "?page_size=" .. self.page_size, "", headers )
	
	--help:print_all(answer)

	Whambush:getUserLoop(answer.result.body,_callback)		
	
	--network.request( self.endpoint .. self.USERS .. "?page_size=" .. self.page_size, "GET", function(e) Whambush:getUserLoop(e,_callback); end, params )

end

function Whambush:getUserLoop ( data , _callback)
	--if ( event.isError ) then
 	--	coronium.error( "Network error!" )
 	--else
 	--	local data = json.decode( event.response )

 	local nextEndpoint = data.next
 	
 	--coronium.log(nextEndpoint)

 	for key,value in pairs(data.results) do
		table.insert( self.userTable,value )
	end
	
	if type(nextEndpoint) == "string" then
		assert( self.token , "Error: No token!" )
			
		local headers = {}
		headers["Content-Type"] = "application/json"
		headers["Authorization"] = "Token " .. self.token

		local params = {}
		params.headers = headers
		
		local answer = coronium.network.getJson( nextEndpoint, "", headers )
	
		Whambush:getUserLoop(answer.result.body,_callback)		
	
		--network.request( nextEndpoint, "GET", function(e) Whambush:getUserLoop(e,_callback); end, params )
	else
		--coronium.log( "Got all users" )
		--help:print_all(self.userTable)
		_callback(self.userTable)
 	end
 	--end
end



-- local function networkListener( event )

-- 	if ( event.isError ) then
-- 		print( "Network error!" )
-- 	else
--         --print_r(json.decode( event.response))
--         --print ( "RESPONSE: " .. event.response )
--         for key,value in pairs(json.decode( event.response)["user"]) do
--         	print( key, value )
--         end

--     end
-- end


function Whambush:init( o )
  	--coronium.log("init")

  	self.username = o.username
  	self.password = o.password
  	if o.api then
		self.api = o.api 
	  	self.endpoint = self.api  
	else
		if o.dev then
			self.api = "http://whambush.net/api/"
		else
			self.api = "https://whambush.com/api/"
		end
		if o.api_version then
			self.api_version = o.api_version 
		end

	 	self.endpoint = self.api .. self.api_version .. "/"

	end
	
	if o.page_size then
		self.page_size = o.page_size
	end


	coronium.log("API: " .. self.endpoint )

	Whambush:authenticate()
  
end

return Whambush
