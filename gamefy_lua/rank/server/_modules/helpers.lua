local helper = {}

--merge tables
function helper:merge(t1, t2)
    for k, v in pairs(t2) do
        if (type(v) == "table") and (type(t1[k] or false) == "table") then
            helper:merge(t1[k], t2[k])
        else
            t1[k] = v
        end
    end
    return t1
end

function helper:print_all( t , deep)
    if (type(t) == "table") then
        local d = deep or 0
        local head = ""
        for i=0,d-1 do
            head = head .. "  "
        end
        for key,value in pairs( t ) do
            coronium.log(head .. key, value )
            if (type(value) == "table") then
                coronium.log(head .. "[")
                helper:print_all(value,d+1)
                coronium.log(head .. "]")
            end
        end
    else
        coronium.log(data)
    end    

end 

function helper:jobFailed(errorTxt)
    
    local html_body = "<body><h2>Rank job failed : " .. errorTxt .. "</h2></body>"
    local plain = "Rank job failed!" .. errorTxt

    local options = {
      subject = "Rank job failed",
      to = { name = "JK", email = "jari@whambush.com" },
      from = { name = "coronium.job", email = "noreply@coronium.whambush.com" },
    }

    local answer = coronium.network.email( html_body, options )
    if answer.error then
        coronium.error("Error in mail: " .. answer.error)
    end
 
    coronium.error("Error: " ..  errorTxt )
end

function helper:updateUserScore( _dbName, _username , _score, _id, _country , _rank )
    local score = coronium.mongo:findObject(_dbName,{username = _username}) -- check if score exists
    if score.error then --no old score, create
        local answer = coronium.mongo:createObject( _dbName, { username = _username, score = _score, userId = _id, country = _country, rank = _rank , rank_local = _rank} )
        return coronium.answer( { score = _score, objectId = answer.result.objectId } )
    else -- old score exists update
        local answer = coronium.mongo:updateObjects( _dbName, { username = _username }, {score = _score, country = _country, rank = _rank, rank_local = _rank} )
        return coronium.answer( { score = _score, objectId = score.result.objectId } )
    end
end

function helper:countTable( t )
    local count = 0
    for v in pairs( t ) do
        count = count + 1
    end
    return count
end


return helper
