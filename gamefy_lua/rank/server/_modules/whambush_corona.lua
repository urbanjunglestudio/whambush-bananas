--[[
Whambush API library
(c) Urban Jungle Studios Oy / Jari Kalinainen
]]--

local http = require "socket.http"
local json = require "json"
local ltn12 = require "ltn12"
local help = require "helpers" 

local Whambush = 
{
	token = nil,
	username = nil,
	password = nil,
	api_version = "v1.3",
	api = nil,
	
	page_size = 10,
	
	--endpoints
	LOGIN = "login/",
	USERS = "users/",

	endpoint = nil,

	userTable = {}
}

function Whambush:authenticate()
	
	local body = {}
	body.username = self.username
	body.password = self.password
	local json_body = json.encode(body)

	assert( self.username ~= nil, "Error: No username" )
	assert( self.password ~= nil, "Error: No password" )
	
	local headers = {}
	headers["Content-Type"] = "application/json"
 	headers["content-length"] = #json_body

	local respbody = {}

	local r, c, h, s = http.request(
	{ 
		url = self.endpoint .. self.LOGIN,
		method = "POST",
		headers = headers,
		source = ltn12.source.string(json_body),
		sink = ltn12.sink.table(respbody)
	}) 
	assert(c < 300,  "Error:" .. c .. " - " ..self.endpoint .. self.LOGIN )
	
	
	--for key,value in pairs(json.decode(table.concat(respbody))) do
	--  	print( key, value )
 	--end
    
    local rspbody = json.decode(table.concat(respbody))
	self.token = rspbody.token
	--print( self.token )

end

function Whambush:getUser( userId , _callback )
	-- body
	assert( self.token , "Error: No auth token!" )
	local headers = {}
	headers["Content-Type"] = "application/json"
 	headers["Authorization"] = "Token " .. self.token

	local params = {}
	params.headers = headers
--	params.body = json.encode(body)
	
	network.request( self.endpoint .. self.USERS .. userId .. "/", "GET", _callback, params )

end

function Whambush:getAllUsers( _callback )
	-- body
	assert( self.token , "Error: No auth token!" )
	
	local headers = {}
	headers["Content-Type"] = "application/json"
 	headers["Authorization"] = "Token " .. self.token

	local params = {}
	params.headers = headers
--	params.body = json.encode(body)
			
	network.request( self.endpoint .. self.USERS .. "?page_size=" .. self.page_size, "GET", function(e) Whambush:getUserLoop(e,_callback); end, params )

end

function Whambush:getUserLoop ( event , _callback)
	if ( event.isError ) then
 		print( "Network error!" )
 	else
 		local data = json.decode( event.response )
		
 		local nextEndpoint = data.next
 
 		for key,value in pairs(data.results) do
			table.insert( self.userTable,value )
		end
	
 		if nextEndpoint then
			assert( self.token , "Error: No token!" )
			
			local headers = {}
			headers["Content-Type"] = "application/json"
		 	headers["Authorization"] = "Token " .. self.token

			local params = {}
			params.headers = headers
		
			network.request( nextEndpoint, "GET", function(e) Whambush:getUserLoop(e,_callback); end, params )
		else
			--print( "Got all users" )
			_callback(self.userTable)
 		end
 	end
end



-- local function networkListener( event )

-- 	if ( event.isError ) then
-- 		print( "Network error!" )
-- 	else
--         --print_r(json.decode( event.response))
--         --print ( "RESPONSE: " .. event.response )
--         for key,value in pairs(json.decode( event.response)["user"]) do
--         	print( key, value )
--         end

--     end
-- end


function Whambush:init( o )
  	print("init")

  	self.username = o.username
  	self.password = o.password
  	if o.api then
		self.api = o.api 
	  	self.endpoint = self.api  
	else
		if o.dev then
			self.api = "http://whambush.net/api/"
		else
			self.api = "https://whambush.com/api/"
		end
		if o.api_version then
			self.api_version = o.api_version 
		end

	 	self.endpoint = self.api .. self.api_version .. "/"

	end
	
	if o.page_size then
		self.page_size = o.page_size
	end


	print( self.endpoint )

	Whambush:authenticate()
  
end

return Whambush
