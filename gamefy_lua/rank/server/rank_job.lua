local wb = require( "_modules.whambush_coronium" )
local help = require "_modules.helpers" 
local g = require("globals")


dbName = ""
dbName1 = ""
dbName2 = ""

countries = {}

--------------------------------------------------------

local function userGot( data )
    help:print_all(data)
end
--------------------------------------------------------


local function updateUserRank( _country_in )
    
    local _country = _country_in or {['$regex'] = ".*"}
    
    ---ranking   
    local answer = coronium.mongo:getObjectCount( dbName , { country = _country , score = { ['$gt'] = 0 }})
    local count
    if answer.error then
        help:jobFailed(answer.error)
        coronium.log(answer.error)
        count = 0
    else
        count = answer.result
    end
    
    local loopCount
    
    if count > 0 then
        local allScores = coronium.mongo:getObjects( dbName, { country = _country , score = { ['$gt'] = 0 }}, count, "score", "DESC")
        
        if (table.getn(allScores.result) < count) then
           coronium.error("Got too few scores from DB")
        end

        --update rank variable to user
        loopCount = #allScores['result']
        
        --coronium.log( "lc: "..loopCount.." c: "..tostring(_country) )


        for i=1,loopCount do
            local tmp = allScores.result[i]
            
            local answer 
            if (_country_in == nil) then
                answer = coronium.mongo:updateObjects( dbName, { username = tmp.username }, {rank = i} )
            else
                answer = coronium.mongo:updateObjects( dbName, { username = tmp.username }, {rank_local = i} )
            end
            if answer.error then
                coronium.error("Error rank: " .. username, answer.error)
            end

        end
    else
        loopCount = 0
    end
   
    --update score = 0
    local zerorank = loopCount + 1
    local answer
    --coronium.log("zr: "..zerorank.." c: "..tostring(_country) )

    if (_country_in == nil) then
        answer = coronium.mongo:updateObjects( dbName, { country = _country, score = 0 }, {rank = zerorank} )
    else         
        answer = coronium.mongo:updateObjects( dbName, { country = _country, score = 0 }, {rank_local = zerorank} )
    end
    if answer.error then
        coronium.error("Error zerorank: " .. username, answer.error)
    end   
    --coronium.log("e")
end
--------------------------------------------------------

local function gotAllUsers( data )
    local count = #data
    --coronium.log( "all users got" )
    for key,value in pairs(data) do
    	local country
        if (type(value['country']) ~= "string") then
    	   country = "FI"
    	else
           country =  value['country']
        end
        countries[country] = 1

    	if (not value['is_guest']) then
            if (tonumber(value['id']) > 1) then
                local tmp = help:updateUserScore( dbName , value['username'], value['userscore'], value['id'], country, count)
    	        if (tmp.error) then
    		        --print( "Error: updateUserScore " ,  value['username'])
    		        help:jobFailed(tmp.error)
    	        end
            end
        end
    end
    
    --rank globally
    updateUserRank( nil )
    
    --rank locals
    --help:print_all(countries)
    --coronium.log( countries )
    --coronium.log( "numofc: ".. help:countTable(countries) )
    if (help:countTable(countries) > 0) then
        for key,value in pairs(countries) do
            --coronium.log( "key: ".. key .. " " .. value)
            updateUserRank( key )
        end
    end
    
    local answer = coronium.mongo:updateObjects( "Config", { tableId = "Config" }, { dbName = dbName, time = os.date("!%Y-%m-%dT%T UTC") } )
    coronium.log("# Rank job ends: " .. os.date("!%d.%m.%Y - %T"))

end
--------------------------------------------------------



coronium.log("# Rank job starts: " .. os.date("!%d.%m.%Y - %T"))

--config
local answer = coronium.mongo:getObjects("Config", { tableId = "Config" }, 1)

if (#answer['result'] < 1) then
    local answer = coronium.mongo:createObject( "Config", { dbName = "Score", dbName1 = "Score",dbName2 = "ScoreA", tableId = "Config", time = os.date("!%Y-%m-%dT%T UTC")} )
    --help:print_all(answer)
    local answer = coronium.mongo:getObjects("Config", { tableId = "Config" }, 1)
end

local result = answer['result'][1]
dbName = result['dbName']
dbName1 = result['dbName1']
dbName2 = result['dbName2']
if (dbName == dbName1) then
    dbName = dbName2    
else
    dbName = dbName1    
end


wb:init({ username = g.username, password = g.password, page_size=50})
wb:getAllUsers(gotAllUsers)

