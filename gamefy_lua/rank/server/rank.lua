local help = require("_modules.helpers")

local in_data = coronium.input()

--help:print_all(in_data)

local page_size = in_data.page_size_debug or 30
local page_number = in_data.page_number or 1
local country = in_data.country or "ZZ" --or {['$regex'] = ".*"}
if (type(country) == "userdata") then
	country = "ZZ"
end
if (type(country) == "string") then
	country = string.upper(country)
end
local newCountry = country
if (country == "ZZ") then
	country = {['$regex'] = ".*"}
end

--coronium.log("country: ".. tostring( country ) .. " : " .. type(country))

if (type(page_number) ~= "number") then
	page_number = tonumber(page_number)
end
if (page_number < 1) then
	page_number = 1
end

local finalAnswer = {}

local answer = coronium.mongo:getObjects("Config", { tableId = "Config" }, 1)
local result = answer['result'][1]
dbName = result['dbName']

finalAnswer['time'] = result['time']

local answer = coronium.mongo:getObjects(dbName, { username = in_data.username }, 1)
local users = answer['result']

if (users[1] == nil) then -- user dont exist create tmp data
	local newUser = {username = in_data.username, userId = 0,  score = 0, country = newCountry}
	
	local answer = coronium.mongo:getObjectCount( dbName , { score = { ['$gt'] = 0 } , country = {['$regex'] = ".*"} })
	newUser['rank'] = answer['result'] + 1
	
	local answer = coronium.mongo:getObjectCount( dbName , { score = { ['$gt'] = 0 } , country = country })
	newUser['rank_local'] = answer['result'] + 1

	finalAnswer['user'] = newUser
else
	finalAnswer['user'] = users[1]
end

local answer = coronium.mongo:getObjectCount( dbName , { score = { ['$gt'] = 0 } , country = country })
finalAnswer['count'] = answer['result']
if (finalAnswer['user']['rank_local'] > finalAnswer['count']) then
	finalAnswer['user']['rank_local'] = finalAnswer['count'] + 1
end
   
local start_index = (page_size * (page_number-1))
local end_index = page_size * page_number

local answer = coronium.mongo:getObjects(dbName, { score = { ['$gt'] = 0 } , country = country }, end_index, "score", "DESC")
local fullResult = answer['result']
local pageResult ={}
for i=1,page_size do
	pageResult[i] = fullResult[i+start_index]
end
if (table.getn(pageResult) > 0) then
	finalAnswer['results'] = pageResult
else
	finalAnswer['results'] = {nil}
end

--pagination
finalAnswer['next_page'] = nil
if (page_number < math.floor(finalAnswer['count']/page_size)+1) then
	finalAnswer['next'] = page_number + 1
end
finalAnswer['prev_page'] = nil
if (page_number > 1) then
	finalAnswer['prev'] = page_number - 1
end

coronium.output( coronium.answer( finalAnswer ) )
