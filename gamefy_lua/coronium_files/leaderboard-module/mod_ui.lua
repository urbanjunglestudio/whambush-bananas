----------------------------------------------------
-- Coronium Leaderboard demo ui module
-- Copyright (c) 2014 develephant
-- http://coronium.io | http://coronium.org
----------------------------------------------------
local widget = require( "widget" )

local function getCredentialsUi( releaseEvent )

	local grp = display.newGroup()

	-- Create the widget
	local registerBtn = widget.newButton
	{
	    left = 70,
	    top = display.contentCenterY - 80,
	    id = "registerButton",
	    label = "Register",
	    fontSize = 45,
	    onRelease = releaseEvent
	}

	-- Create the widget
	local loginBtn = widget.newButton
	{
	    left = 70,
	    top = display.contentCenterY + 30,
	    id = "loginButton",
	    label = "Login",
	    fontSize = 45,
	    onRelease = releaseEvent
	}

	grp:insert( registerBtn )
	grp:insert( loginBtn )

	return grp

end

local function getRegisterUi( releaseEvent )

	local grp = display.newGroup()

	local usernameField = native.newTextField( display.contentCenterX, display.contentCenterY - 60, 180, 30 )
	usernameField.id = "username"
	usernameField.placeholder = "Username"

	local passwordField = native.newTextField( display.contentCenterX, display.contentCenterY, 180, 30 )
	passwordField.id = "password"
	passwordField.isSecure = true
	passwordField.placeholder = "Password"

	local passwordField2 = native.newTextField( display.contentCenterX, display.contentCenterY  + 40, 180, 30 )
	passwordField2.id = "password2"
	passwordField2.isSecure = true
	passwordField2.placeholder = "Confirm Password"

	local submitBtn = widget.newButton
	{
	    left = display.contentCenterX - 35,
	    top = display.contentCenterY + 60,
	    id = "registerButton",
	    label = "Submit",
	    fontSize = 24,
	    onRelease = releaseEvent
	}

	grp:insert( usernameField )
	grp:insert( passwordField )
	grp:insert( passwordField2 )
	grp:insert( submitBtn )

	grp.usernameField = usernameField
	grp.passwordField = passwordField
	grp.passwordField2 = passwordField2

	return grp

end

local function getLoginUi( releaseEvent )

	local grp = display.newGroup()

	local usernameField = native.newTextField( display.contentCenterX, display.contentCenterY - 30, 180, 30 )
	usernameField.id = "username"
	usernameField.placeholder = "Username"

	local passwordField = native.newTextField( display.contentCenterX, display.contentCenterY + 30, 180, 30 )
	passwordField.id = "password"
	passwordField.isSecure = true
	passwordField.placeholder = "Password"

	local submitBtn = widget.newButton
	{
	    left = display.contentCenterX - 30,
	    top = display.contentCenterY + 80,
	    id = "registerButton",
	    label = "Login",
	    fontSize = 24,
	    onRelease = releaseEvent
	}

	grp:insert( usernameField )
	grp:insert( passwordField )
	grp:insert( submitBtn )

	grp.usernameField = usernameField
	grp.passwordField = passwordField

	return grp

end

local function getScoreUi( releaseEvent )

	local grp = display.newGroup()

	local scoreBtn = widget.newButton
	{
	    left = 80,
	    top = display.contentCenterY -20,
	    id = "scoreButton",
	    label = "Score!",
	    fontSize = 24,
	    onRelease = releaseEvent
	}

	local viewBtn = widget.newButton
	{
	    left = 80,
	    top = display.contentCenterY + 40,
	    id = "leaderboardButton",
	    label = "Leaderboard",
	    onRelease = releaseEvent
	}

	grp:insert( scoreBtn )
	grp:insert( viewBtn )

	return grp

end

local function getLeaderboardUi( dataProvider, releaseEvent )

	local grp = display.newGroup()

	local function onRowRender( event )

	    -- Get reference to the row group
	    local row = event.row

	    -- Cache the row "contentWidth" and "contentHeight" because the row bounds can change as children objects are added
	    local rowHeight = row.contentHeight
	    local rowWidth = row.contentWidth

	    local username = dataProvider[row.index].username
	    local score = dataProvider[row.index].score
	    local platform = dataProvider[row.index].platform
	    local country = dataProvider[row.index].country

	    local label = username .. " " .. score

	    local rowTitle = display.newText( row, label, 0, 0, nil, 14 )
	    rowTitle:setFillColor( 1 )

	    -- Align the label left and vertically centered
	    rowTitle.anchorX = 0
	    rowTitle.x = 24
	    rowTitle.y = rowHeight * 0.5
	end

	-- Create the widget
	local tableView = widget.newTableView
	{
	    left = 0,
	    top = 0,
	    height = display.contentHeight - 40,
	    width = display.contentWidth,
	    hideBackground = true,
	    onRowRender = onRowRender,
	    onRowTouch = onRowTouch,
	    listener = scrollListener
	}

	-- Insert 40 rows
	for i = 1, #dataProvider do
	    -- Insert a row into the tableView
	    tableView:insertRow({
    		rowColor = { default={ 0 }, over={ 0 } }
	    })
	end

	local backBtn = widget.newButton
	{
	    left = 80,
	    top = display.contentHeight - 24,
	    id = "backButton",
	    label = "Done",
	    onRelease = releaseEvent
	}

	grp.tableView = tableView

	grp:insert( tableView )
	grp:insert( backBtn )

	return grp
end

local mod = {
	getCredentialsUi = getCredentialsUi,
	getRegisterUi = getRegisterUi,
	getLoginUi = getLoginUi,
	getScoreUi = getScoreUi,
	getLeaderboardUi = getLeaderboardUi
}

return mod