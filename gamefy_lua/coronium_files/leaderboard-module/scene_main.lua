----------------------------------------------------
-- Coronium Leaderboard demo
-- Copyright (c) 2014 develephant
-- http://coronium.io | http://coronium.org
----------------------------------------------------
local globals = require( "globals" )
local ui = require( "mod_ui" )
local coronium = require( "mod_coronium" )
local leaderboard = require( "mod_leaderboard" )
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

coronium:init( { appId = globals.appid, apiKey = globals.apikey } )
leaderboard.init( coronium )

local userId = nil
---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
function scene.clearSceneGroup()
	for i = scene.view.numChildren, 1, -1 do
		scene.view:remove( i )
	end
end

function scene.renderLeaderboardUi()

	local uiGrp

	scene.clearSceneGroup()

	native.setActivityIndicator( true )

	leaderboard.getScores( {}, function(e)

		native.setActivityIndicator( false )

		if not e.error then
			local dataProvider = e.result

			local function releaseEvent( event )
				uiGrp.tableView:removeSelf()
				scene.renderScoreUi()
			end
			
			uiGrp = ui.getLeaderboardUi( dataProvider, releaseEvent )
			scene.view:insert( uiGrp )
		else
			native.showAlert( "Leaderboard", "Could not find data", { "OK" }, function(e)
				scene.renderScoreUi()
			end)
		end
	end)

end

function scene.renderScoreUi()

	local uiGrp

	scene.clearSceneGroup()

	local function releaseEvent( event )

		local btnId = event.target.id

		if btnId == "scoreButton" then
			local randomScore = math.floor( math.random() * 10000 )
			leaderboard.addScore( userId, randomScore )
		elseif btnId == "leaderboardButton" then
			scene.renderLeaderboardUi()
		end

	end

	uiGrp = ui.getScoreUi( releaseEvent )
	scene.view:insert( uiGrp )
end

function scene.loginUser()

	local uiGrp

    scene.clearSceneGroup()

	local function releaseEvent( event )
		local username = uiGrp.usernameField.text
		local password = uiGrp.passwordField.text

		native.setActivityIndicator( true )

		leaderboard.getUser( username, password, function(e)

			native.setActivityIndicator( false )

			if not e.error then
				userId = e.result.objectId
				scene.renderScoreUi()
			else
				uiGrp.usernameField.text = ""
				uiGrp.passwordField.text = ""

				native.showAlert( "Leaderboard", e.error, { "OK" } )
			end
		end)

	end

	uiGrp = ui.getLoginUi( releaseEvent )
	scene.view:insert( uiGrp )
end

function scene.registerUser()

	local uiGrp

    scene.clearSceneGroup()

	local function releaseEvent( event )
		local username = uiGrp.usernameField.text
		local password = uiGrp.passwordField.text
		local password2 = uiGrp.passwordField2.text

		native.setActivityIndicator( true )

		leaderboard.addUser( username, password, function(e) 

			native.setActivityIndicator( false )
			if not e.error then
				userId = e.result.objectId
				scene.renderScoreUi()
			else
				uiGrp.usernameField.text = ""
				uiGrp.passwordField.text = ""
				uiGrp.passwordField2.text = ""

				native.showAlert( "Leaderboard", e.error, { "OK" } )
			end
		end)

	end

	uiGrp = ui.getRegisterUi( releaseEvent )
	scene.view:insert( uiGrp )
end

function scene.chooseCredentials()

	local uiGrp

    scene.clearSceneGroup()

	local function releaseEvent( event )
		local btnId = event.target.id
		if btnId == "registerButton" then
			scene.registerUser()
		elseif btnId == "loginButton" then
			scene.loginUser()
		end
	end
	uiGrp = ui.getCredentialsUi( releaseEvent )
	scene.view:insert( uiGrp )
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
    local group = self.view

    --if no user id, prompt for login.
    if not userId then
    	scene.chooseCredentials()
    else
    	scene.renderScoreUi()
    end
end
---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

---------------------------------------------------------------------------------

return scene
    