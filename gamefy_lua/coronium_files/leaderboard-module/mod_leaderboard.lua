----------------------------------------------------
-- Coronium Leaderboard client module
-- Copyright (c) 2014 develephant
-- http://coronium.io | http://coronium.org
----------------------------------------------------
local this = {
	coronium = nil
}

function this.addUser( username, password, _callback )
	this.coronium:run( "leaderboard", { action = "addUser", username = username, password = password }, _callback ) 
end

function this.getUser( username, password, _callback )
	this.coronium:run( "leaderboard", { action = "getUser", username = username, password = password }, _callback )
end

function this.addScore( objectId, score, _callback )
	local platform = this.coronium:getPlatform()
	this.coronium:run( "leaderboard", { action = "addScore", objectId = objectId, score = score, platform = platform }, _callback )
end

function this.getScores( optionsTable, _callback )
	local optionsTable = optionsTable or {}
	optionsTable.limit_fields = { userId = 0, objectId = 0 } 
	optionsTable.action = "getScores"
	this.coronium:run( "leaderboard", optionsTable, _callback )
end

function this.getScoresByUsername( username, optionsTable, _callback )
	local optionsTable = optionsTable or {}
	optionsTable.query = { username = username }
	optionsTable.action = "getScores"
	this.coronium:run( "leaderboard", optionsTable, _callback )
end

function this.getScoresByUserId( userId, optionsTable, _callback )
	local optionsTable = optionsTable or {}
	optionsTable.query = { userId = userId }
	optionsTable.action = "getScores"
	this.coronium:run( "leaderboard", optionsTable, _callback )
end

function this.getScoresByPlatform( platform, optionsTable, _callback )
	local optionsTable = optionsTable or {}
	optionsTable.query = { platform = platform }
	optionsTable.action = "getScores"
	this.coronium:run( "leaderboard", optionsTable, _callback )
end

function this.getScoresByCountry( countryCode, optionsTable, _callback )
	local optionsTable = optionsTable or {}
	optionsTable.query = { country = countryCode }
	optionsTable.action = "getScores"
	this.coronium:run( "leaderboard", optionsTable, _callback )
end

function this.getScoresGreater( scoreLimit, optionsTable, _callback )
	local optionsTable = optionsTable or {}
	optionsTable.query = { score = { ["$gt"] = scoreLimit } }
	optionsTable.action = "getScores"
	this.coronium:run( "leaderboard", optionsTable, _callback )
end

function this.getScoresLess( scoreLimit, optionsTable, _callback )
	local optionsTable = optionsTable or {}
	optionsTable.query = { score = { ["$lt"] = scoreLimit } }
	optionsTable.action = "getScores"
	this.coronium:run( "leaderboard", optionsTable, _callback )
end

function this.getScoresEqual( scoreLimit, optionsTable, _callback )
	local optionsTable = optionsTable or {}
	optionsTable.query = { score = scoreLimit }
	optionsTable.action = "getScores"
	this.coronium:run( "leaderboard", optionsTable, _callback )
end

function this.init( coronium_instance )
	this.coronium = coronium_instance
end

return this
