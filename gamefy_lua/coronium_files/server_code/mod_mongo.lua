---------------------------------------------------------------
--Coronium Mongo DB Module
--(c)2014 Chris Byerley - develephant
---------------------------------------------------------------

local t = {}

--sorting
t.ASC = 1
t.DESC = -1

--stored db params
t.database = nil
t.collection = nil

--current db instance
t.db = nil 

function t:connect()
	local mongo = require( "mongol" )
	conn = mongo:new()
	conn:set_timeout(2500)
	ok, err = conn:connect( "127.0.0.1" )
	if not ok then
	  return err
	else
		return conn
	end
end

--GENERAL
function t:useDatabase( db_name )
	local connection = self:connect()
	if connection then
    	self.db = connection:new_db_handle( db_name )
	else
		return nil
	end
    return true
end

function t:getCollection( collectionName ) 
	return self.db:get_col( collectionName )
end

function t:getCollectionCount( db, collectionName, query )
	if ( self:useDatabase( db ) ) then
		local query = query or {}
		local collection = self:getCollection( collectionName )
		local count = collection:count( query )
		return coronium.answer( count )
	else
		return coronium.error( "db connection error" )
	end
end

-- qtype = Insert, Update, Delete, Select
function t:query( q_type, q_table, data_table, options, collection, database )

	local q_type = string.lower( q_type )

	local database = database or self.database
	local collection = collection or self.collection
	local data_table = data_table or {}
	local options = options or {}

	local answer = nil

	if q_type == "insert" then

		answer = self:insert( database, collection, q_table )

	elseif q_type == "update" then

		local upsert = options.upsert or 0
		local multiupdate = options.multiupdate or 0

		local data_table = { ["$set"] = data_table }

		answer = self:update( database, collection, q_table, data_table, upsert, multiupdate, 1 )

	elseif q_type == "delete" then

		local delete_one = data_table.single or 0
		answer = self:delete( database, collection, q_table, delete_one )

	elseif q_type == "select" then

		local filterFields = options.filterFields or {}
		local limit = options.limit or 100
		local sortField = options.sortField or nil
		local sortOrder = options.sortOrder or "ASC"

		answer = self:find( database, collection, q_table, filterFields, limit, sortField, sortOrder )

	end

	return answer

end

function t:findOne( db, collectionName, query, filterFields )
	if ( self:useDatabase( db ) ) then
		local filterFields = filterFields or {}
		
		--== Check if there are any inclusion fields
		local is_inclusion = false
		for key, value in pairs( filterFields ) do
			if value == 1 then
				is_inclusion = true
				break
			end
		end
		--== If so, cancel any exclusion
		if is_inclusion then
			for key, value in pairs( filterFields ) do
				if value == 0 then
					filterFields[ key ] = nil
				end
			end
		end

		filterFields._id = 0
		
		local collection = self:getCollection( collectionName )
		local result = collection:find_one( query, filterFields )
		if result then
			return coronium.answer( result )
		else
			return coronium.error( "find one failed: " .. collectionName )
		end
	else
		return coronium.error( "db connection error" )
	end
end

function t:find( db, collectionName, query, filterFields, limit, sortField, sortOrder )
	if ( self:useDatabase( db ) ) then
		local limit = tonumber( limit ) or 100
		local filterFields = filterFields or {}

		--== Check if there are any inclusion fields
		local is_inclusion = false
		for key, value in pairs( filterFields ) do
			if value == 1 then
				is_inclusion = true
				break
			end
		end
		--== If so, cancel any exclusion
		if is_inclusion then
			for key, value in pairs( filterFields ) do
				if value == 0 then
					filterFields[ key ] = nil
				end
			end
		end

		filterFields._id = 0

		local collection = self:getCollection( collectionName )

		local results = collection:find( query, filterFields, 0 )

		if not results then
			return coronium.error( "no results found" )
		end

		local records = {}
		
		if sortOrder == "ASC" then

			if sortField then
				local s = {}
				s[sortField] = 1
				results:sort( s )
			end

			if limit then
				results:limit( limit )
			end

			for _, record in results:pairs() do
				table.insert( records, record )
			end

		elseif sortOrder == "DESC" then

			if sortField then
				local s = {}
				s[sortField] = -1
				results:sort( s )
			end

			if limit then
				results:limit( limit )
			end

			for _, record in results:pairs() do
				table.insert( records, record )
			end


		else
			if limit then
				results:limit( limit )
			end

			for _, record in results:pairs() do
				table.insert( records, record )
			end
		end

		--coronium.tprint( records )

		return coronium.answer( records )
	else
		return coronium.error( "db connection error" )
	end
end

function t:insert( db, collectionName, dataTable )
	if ( self:useDatabase( db ) ) then
		local collection = self:getCollection( collectionName )
		local n, err = collection:insert( { dataTable }, 0, 1 )
		if n then 
			return coronium.answer( n ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection error" )
	end

end

function t:update( db, collectionName, selectDataTable, updateDataTable, upsert, multiupdate, safe )
	if ( self:useDatabase( db ) ) then
		local upsert = upsert or 0
		local multiupdate = multiupdate or 0
		local safe = safe or 1
		local collection = self:getCollection( collectionName )
		local n, err = collection:update( selectDataTable, updateDataTable, upsert, multiupdate, safe )
		if n then
			return coronium.answer( n ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection error" )
	end
end

function t:delete( db, collectionName, selectDataTable, singleRemove )
	if ( self:useDatabase( db ) ) then
		local singleRemove = singleRemove or 0
		local collection = self:getCollection( collectionName )

		local n, err = collection:delete( selectDataTable, singleRemove, 1 )
		if n then
			return coronium.answer( n ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection error" )
	end
end

--OBJECTS API
function t:getObjectCount( collectionName, query )
	if ( self:useDatabase( "_objects" ) ) then
		local query = query or {}
		local collection = self:getCollection( collectionName )
		return coronium.answer( collection:count( query ) )
	else
		return coronium.error( "db connection error" )
	end
end

function t:createObject( collectionName, dataTable )
	if( self:useDatabase( "_objects" ) ) then
		local collection = self:getCollection( collectionName )
	    if not dataTable.objectId then
	      	dataTable.objectId = coronium.newUUID()
	    end
		local n, err = collection:insert( { dataTable } )
		if n then
			return coronium.answer( { objectId = dataTable.objectId } )
		else
			return coronium.error( err )
		end
	else
		return coronium.error( "db connection error" )
	end
end

function t:createObjects( collectionName, dataTableArr )
	if ( self:useDatabase( "_objects" ) ) then
		local collection = self:getCollection( collectionName )
		for i=1, #dataTableArr do
			local id = coronium.newUUID(i)
		    if not dataTableArr[i].objectId then
		      dataTableArr[i].objectId = id
		    end
		end
		local n, err = collection:insert( dataTableArr )
		if n then
			return coronium.answer( #dataTableArr .. " Objects created" )
		else
			return coronium.error( err )
		end
	else
		return coronium.error( "db connection error" )
	end
end

function t:getObject( collectionName, objectId, limitFields )
	if ( self:useDatabase( "_objects" ) ) then
		local limitFields = limitFields or {}
		limitFields._id = 0
		local collection = self:getCollection( collectionName )
		local record = collection:find_one( { objectId = objectId }, limitFields )
		if record then
			return coronium.answer( record )
		else
			return coronium.error( "find one failed: " .. collectionName )
		end
	else
		return coronium.error( "db connection error" )
	end
end

function t:findObject( collectionName, query, limitFields )
	if ( self:useDatabase( "_objects" ) ) then
		local limitFields = limitFields or {}
		limitFields._id = 0
		local collection = self:getCollection( collectionName )
		local record = collection:find_one( query, limitFields )
		if record then
			return coronium.answer( record )
		else
			return coronium.error( "find one failed: " .. collectionName )
		end
	else
		return coronium.error( "db connection error" )
	end
end

function t:getObjects( collectionName, query, limit, sortField, sortOrder, limitFields )

	if ( self:useDatabase( "_objects" ) ) then

		local limit = tonumber( limit ) or 100
		local limitFields = limitFields or {}

		limitFields._id = 0

		local sortOrder = sortOrder or "ASC"

		local collection = self:getCollection( collectionName )
		local results = collection:find( query, limitFields, 0 )

		if not results then
			return coronium.error( "no results found" )
		end

		local records = {}

		if sortOrder == "ASC" then

			if sortField then
				local s = {}
				s[sortField] = 1
				results:sort( s )
			end

			if limit then
				results:limit( limit )
			end

			for _, record in results:pairs() do
				table.insert( records, record )
			end

		elseif sortOrder == "DESC" then

			if sortField then
				local s = {}
				s[sortField] = -1
				results:sort( s )
			end

			if limit then
				results:limit( limit )
			end

			for _, record in results:pairs() do
				table.insert( records, record )
			end

		else

			if limit then
				results:limit( limit )
			end
			
			for _, record in results:pairs() do
				table.insert( records, record )
			end
		end
		return coronium.answer( records )

	else
		return coronium.error( "db connection error" )
	end
end

function t:updateObject( collectionName, objectId, updateObject, isUpsert )
	if ( self:useDatabase( "_objects" ) ) then
		local isUpsert = isUpsert or 0
		local record = { objectId = objectId }
		local update = { ["$set"] = updateObject }
		local collection = self:getCollection( collectionName )
		local n, err = collection:update( record, update, isUpsert, 0, 1 )
		if n then
			return coronium.answer( n ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection failed" )
	end
end

function t:updateObjects( collectionName, queryObject, updateObject, isUpsert )
	if ( self:useDatabase( "_objects" ) ) then
		local isUpsert = isUpsert or 0
		local update = { ["$set"] = updateObject }
		local collection = self:getCollection( collectionName )
		local n, err = collection:update( queryObject, update, isUpsert, 1, 1 )
		if n then
			return coronium.answer( n ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection failed" )
	end
end

function t:incObjectKeys( collectionName, objectId, updateObject, isUpsert )
	if ( self:useDatabase( "_objects" ) ) then
		local isUpsert = isUpsert or 0
		local record = { objectId = objectId }
		local update = { ["$inc"] = updateObject }
		local collection = self:getCollection( collectionName )
		local n, err = collection:update( record, update, isUpsert, 0, 1 )
		if n then
			return coronium.answer( n ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection failed" )
	end
end

function t:deleteObject( collectionName, objectId )
	if( self:useDatabase( "_objects" ) ) then
		local collection = self:getCollection( collectionName )
		local n, err = collection:delete( { objectId = objectId }, 1 )
		if n then
			return coronium.answer( n ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection failed" )
	end
end

function t:deleteObjects( collectionName, query )
	if ( self:useDatabase( "_objects" ) ) then
		local collection = self:getCollection( collectionName )
		local n, err = collection:delete( query )
		if n then
			return coronium.answer( n ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection failed" )
	end
end

function t:removeObjectKeys( collectionName, objectId, keysToRemove )
	if ( self:useDatabase( "_objects" ) ) then
		local collection = self:getCollection( collectionName )
		--set up removals
		local removals = {}
		for _, v in ipairs( keysToRemove ) do
			removals[v] = 1
		end
		local record = { objectId = objectId }
		local unset = { ["$unset"] = removals }
		local n, err = collection:update( record, unset, 0, 0, 1 )
		if n then
			return coronium.answer( n ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection failed" )
	end
end

function t:dropObjects( collectionName )
	if ( self:useDatabase( "_objects" ) ) then
		local collection = self:getCollection( collectionName )
		local ok, err = collection:drop()
		if ok then
			return coronium.answer( ok ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection failed" )
	end
end

function t:dropCollection( db, collectionName )
	if ( self:useDatabase( db ) ) then
		local collection = self:getCollection( collectionName )
		local ok, err = collection:drop()
		if ok then
			return coronium.answer( ok ) 
		else
			return coronium.error( err ) 
		end
	else
		return coronium.error( "db connection failed" )
	end
end

function t:dropDatabase( db )
	if ( self:useDatabase( db ) ) then
		self.db:dropDatabase()
		return coronium.answer( "database " .. db .. " dropped")
	else
		return coronium.error( "db connection failed" )
	end
end

function t:showCollections( db )
	if ( self:useDatabase( db ) ) then
		return coronium.answer( self.db:list() )
	else
		coronium.error( "db connection failed" )
	end
end

return t