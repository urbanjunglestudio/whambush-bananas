-------------------------------------------------------------
-- Coronium Composer
-------------------------------------------------------------
local globals = require( "globals" )
local coronium = require( "mod_coronium" )

--== Init Coronium
coronium:init({ appId = globals.appId, apiKey = globals.apiKey })

--== Store in globals module
globals.coronium = coronium

--== Start composer
local composer = require( 'composer' )
composer.gotoScene( "scene_main" )
