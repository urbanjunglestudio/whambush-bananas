--======================================================================--
--== Scene Main
--======================================================================--
--== Load composer
local composer = require( "composer" )
local scene = composer.newScene()

--== Load globals module
local globals = require( 'globals' )

--== Localize Coronium
local coronium = globals.coronium
coronium.showStatus = true

--======================================================================--
--== Composer handlers
--======================================================================--
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
	    --======================================================================--
	    --== Coronium App Opened
	    --======================================================================--
	    coronium:appOpened()

    elseif ( phase == "did" ) then
        --======================================================================--
        --== Coronium Log In User
        --======================================================================--
        coronium:loginUser( 'user@email.com', '1234', function( e )
        		if not e.error then
        			print( "I'm logged in!" )
        		end
        end )

    end
end
-- -------------------------------------------------------------------------------
-- Listener setup
scene:addEventListener( "show", scene )
-- -------------------------------------------------------------------------------

return scene