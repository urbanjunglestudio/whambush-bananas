local globals = require( "globals" )
local mc = require( "mod_coronium" )

mc:init({ appId = globals.appId, apiKey = globals.apiKey })

mc.showStatus = true
----------------------------------------------------------------------
-- App Opened
----------------------------------------------------------------------

-- mc:appOpened()

----------------------------------------------------------------------
-- Data Objects
----------------------------------------------------------------------

-- mc:createObject( "friends", { username = "Sandy", im = "sand27", age = 27 }, function(e) 
-- 	mc:getObject( "friends", e.result.objectId, function(e)
--   	print( e.result.username, e.result.age )
-- 	end)
-- end)

----------------------------------------------------------------------
-- Installations
----------------------------------------------------------------------

-- mc:addInstallation( { deviceToken = "DEVICE_TOKEN" } )

-- mc:removeInstallation( "INSTALL_ID", function(e)
--   print( e.result )
-- end)

----------------------------------------------------------------------
-- Server-Side Lua OK
----------------------------------------------------------------------

-- mc:run( "hello" )

----------------------------------------------------------------------
-- Server-Side Lua Error
----------------------------------------------------------------------

-- mc:run( "error" )

----------------------------------------------------------------------
-- Server-Side Lua MySQL
----------------------------------------------------------------------

-- mc:run( "mysql" )

----------------------------------------------------------------------
-- Server-Side Lua PHP
----------------------------------------------------------------------

-- mc:run( "php", { username = "Danny" }, function(e)
-- 	print( e.result.greeting )
-- end)

----------------------------------------------------------------------
