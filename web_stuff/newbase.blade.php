<!DOCTYPE html>
<html lang="fi">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta name="description" content="Whambush on mobiilisovellus, joka on täynnä hauskoja videoita ja päättömiä tehtäviä">
    <meta name="author" content="Urban Jungle Studios Oy">
    <meta property="og:title" content="Whambush" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="http://whambush.com/img/fb.jpg" />
    <meta property="og:description" content="Whambush on mobiilisovellus, joka on täynnä hauskoja videoita ja päättömiä tehtäviä" />

    <title>Whambush</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/default.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('/img/apple-touch-icon-144-precomposed.png') }}">
    <link rel="shortcut icon" href="{{ asset('/img/favicon.png') }}">

    {!! env('HEAD_SCRIPT', '') !!}
</head>

<body>
    @yield('content')

    <div style="background:#303340; color:#999; text-align:center; padding:15px; font-size:12px;">
        <p itemscope itemprop="affiliation">
            <a href="faq/">FAQ</a>
            | <a href="terms-of-service/">Käyttöehdot</a>
            | <a href="mailto:support@whambush.com">Anna palautetta</a>
            | &copy; 2014 Copyright <a href="http://ujs.fi">Urban Jungle Studios Oy</a>
        </p>
    </div>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('/js/jquery.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
</body>

</html>
