<?php
/*
 * zoho support to slackbot (via mandrillapp) notifier
 * by Jari Kalinainen
 */

	function http_post($message)
	{
		$channel = "channel=%23support";
		$slackBot = "https://urbanjunglestudios.slack.com/services/hooks/slackbot?token=Et1z0SGF1C1W7Mxk5TlxyzEF&".$channel;
	   
	 	$options = array(
	    	'http' => array(
	        	'method'  => 'POST',
	        	'content' => $message,
	    	),
	  	);
	  $context  = stream_context_create($options);
	  $result = file_get_contents($slackBot, false, $context);

	  return $result;
	}
	
	$json = $_REQUEST['mandrill_events'];
	$jsonArray = json_decode($json,true);

	preg_match('/^.*href=\"(.*support.zoho.com.*?)\"/m', $jsonArray[0]['msg']['html'], $matches);
	$message = $jsonArray[0]['msg']['text'].PHP_EOL;
	$message .= $matches[1].PHP_EOL;
	
	if (strlen($message) > 10) {
		echo http_post($message).PHP_EOL;
	} else {
		echo "error".PHP_EOL;
	}
	
?>